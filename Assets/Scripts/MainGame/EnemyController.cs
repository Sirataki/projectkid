﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private Vector3 moveDirection;
    [SerializeField] private float gravity = -9.81f;

    [SerializeField] private ParticleSystem psCrator;
    private bool crator = true;
    CharacterController cCon;
    // Use this for initialization
    void Start()
    {
        cCon = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (cCon.isGrounded)
        {
            if (crator)
            {
                psCrator.Play();
                crator = false;
            }
        }
        if (!cCon.isGrounded)
        {
            moveDirection.y += gravity;
        }
        cCon.Move(moveDirection * Time.deltaTime);
    }
}
